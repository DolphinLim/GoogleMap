/**
 * Developer Dolphin Lim
 * **/

class GoogleMap {
    constructor(map, cities, lat, lng, zoom, style, image, arrayPosition) {
        this.map = map;
        this.cities = cities;
        this.lat = lat;
        this.lng = lng;
        this.zoom = zoom;
        this.style = style;
        this.image = image;
        this.arrayPosition = arrayPosition;

        this.initGMap();
    }

    /** Инициализация карты и создание маркеров **/
    initGMap() {
        var map = new google.maps.Map(this.map, {
            center: {lat: this.lat, lng: this.lng},
            zoom: this.zoom,
            styles: this.style
        });      

        for (var index = 0; index < this.arrayPosition.length; index++) {
            
            this.appendMarker(
                {
                    lat: this.arrayPosition[index]['lat'], 
                    lng: this.arrayPosition[index]['lng']
                }, 
                map, 
                this.arrayPosition[index]['title'],
                this.image,
            );

            /** Создание элемента **/
            var link = document.createElement('div');
            link.className = 'link-marker';
            link.innerHTML = this.arrayPosition[index]['city'];
            link.setAttribute('numberMarker', index);
            link.addEventListener('click', (event) => {
                map.setCenter({
                    lat: this.arrayPosition[event.srcElement.getAttribute('numberMarker')].lat, 
                    lng: this.arrayPosition[event.srcElement.getAttribute('numberMarker')].lng
                });                      
            });

            this.cities.appendChild(link);
        }
    }

    /** Метод для создания маркера + окно маркера **/
    appendMarker(position, map, title, image) {
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: title,
            icon: image,
        });

        var infowindow = new google.maps.InfoWindow({
            content: title
        });

        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
    }
}